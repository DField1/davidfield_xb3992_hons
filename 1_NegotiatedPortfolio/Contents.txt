Include:
- Final Product
- PDF document to both Online Portfolio and GIT repo.

MAY Include:
- Asset Folder (as part of Final Product)
- Build (as part of Final Product)
- Project Folder (as part of Final Product)
- Project Plan

Do NOT Include:
- Old Builds
- Old Projects